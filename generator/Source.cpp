#include "../MyString.h"
#include "../Data.h"
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;

const char* file_output_name = "../input.txt";
MyString lastname[22] = { "Alekseev", "Aleksandrov", "Andreev", "Borisov", "Vasyliev", "Gavrilov", "Grigoryev", "Danilov", "Denisov", "Egorov", "Ivanov", "Ilin", "Kirillov", "Kostin", "Maksimov", "Mikhailov", "Nikolaev", "Pavlov", "Romanov", "Semenov", "Tarasov", "Fedorov" };
MyString firstname_f[22] = { "Anna", "Elena", "Veronika", "Vera", "Galina", "Nastya", "Darya", "Eva", "Ekaterina", "Janna", "Elizaveta", "Irina", "Inna", "Larisa", "Ludmila", "Margarita", "Olga", "Oksana", "Sonya", "Sofiya", "Julia", "Nadejda" };
MyString firstname_m[22] = { "Aleksey", "Aleksandr", "Andrey", "Boris", "Vasylii", "Gavril", "Grigorii", "Daniil", "Denisv", "Egor", "Ivan", "Ilya", "Kirill", "Kostya", "Maksim", "Mikhail", "Nikolay", "Pavel", "Roman", "Semen", "Taras", "Fedor" };

int main(int argc, char * argv[]) {
	srand(time(NULL));
	ofstream file_output;
	file_output.open(file_output_name, ios::out);
	file_output.clear();
	int num = (argc == 1) ? 1000 : atoi(argv[1]);

	bool g = true;
	MyString fn, ln;

	for (int i = 0; i < num; i++){
		g = rand() % 2;
		fn = g ? firstname_m[rand() % 22] : firstname_f[rand() % 22];
		ln = g ? lastname[rand() % 22] : lastname[rand() % 22] + "a";
		student(fn, ln, rand() % 100 + 1, g, rand() % 100, rand() % 100, rand() % 100).write(file_output);
	}

	file_output.close();
	system("pause");
	return 0;
}