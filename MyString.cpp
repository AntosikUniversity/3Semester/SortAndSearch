#ifndef __MY_STRING__
#include "MyString.h"
#endif // __MY_STRING__

char * gorn(int number) {
	int num = number, n = 0, NumberOfDigits = 0;
	if (num < 0) num *= -1;

	n = num;
	char *r = new char[256];
	while (n) {
		n /= 10; NumberOfDigits++;
	}
	while (NumberOfDigits--) r++;
	*r = 0;
	while (num) {
		*--r = num % 10 + '0';
		num /= 10;
	}
	return r;
}


/* init */
// ����������� �� �����
MyString::MyString(int number) {
	char * res = gorn(number);
	_length = strlen(res);
	_data = new char[_length + 1];
	strcpy(_data, res);
	delete res;
}

MyString::MyString(bool b) {
	if (b) {
		_length = 4;
		_data = new char[_length + 1];
		strcpy(_data, "true");
	} else {
		_length = 5;
		_data = new char[_length + 1];
		strcpy(_data, "false");
	}
}

// ����������� �� ������� char
MyString::MyString(char * x) {
	if (x) {
		_length = strlen(x);
		_data = new char[_length + 1];
		strcpy(_data, x);
	} else {
		_data = NULL;
		_length = 0;
	}
}

// ����������� �����
MyString::MyString(const MyString & x) {
	if (x._length > 0) {
		_length = x._length;
		_data = new char[_length + 1];
		strcpy(_data, x._data);
	} else {
		_length = 0;
		_data = NULL;
	}
}


/* get & set */
// �������� ������ ��������
char * MyString::chars() const {
	return _data;
}

// �������� ����� �����
int MyString::length() const {
	return _length;
}

// ������������� ����� ����� 
void MyString::setlength(int len) {
	char * temp = new char[len + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	_length = len;
	strncpy(temp, _data, _length);
	delete[] _data;
	_data = temp;
}

MyString & MyString::insert(char newchar) {
	this->insert(_length, newchar);
	return *this;
}

MyString & MyString::insert(char * chars) {
	this->insert(_length, chars);
	return *this;
}

MyString & MyString::insert(MyString str) {
	this->insert(_length, str);
	return *this;
}

MyString & MyString::insert(bool b) {
	this->insert(_length, (MyString)b);
	return *this;
}

MyString & MyString::insert(int num) {
	this->insert(_length, (MyString)num);
	return *this;
}

// ������ �����
void MyString::clear() {
	delete[] _data;
	_data = NULL;
	_length = 0;
}

// ��������� ������ "c"  �� ������� "pos"
MyString & MyString::insert(int pos, char c) {
	int len = _length + 1;
	char tchar[2];
	tchar[0] = c;
	tchar[1] = '\0';
	char * temp = new char[_length + 1 + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, pos);
	strncpy(temp + pos, tchar, 1);
	strncpy(temp + pos + 1, _data + pos - 1 + 1, _length - pos);
	_length += 1;
	delete[] _data;
	_data = temp;
	return *this;
}

// ��������� ������ �������� "chars" �� ������� "pos"
MyString & MyString::insert(int pos, char * chars) {
	int len = _length + strlen(chars);
	char * temp = new char[_length + strlen(chars) + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, pos);
	strncpy(temp + pos, chars, strlen(chars));
	strncpy(temp + pos + strlen(chars), _data + pos - 1 + strlen(chars), _length - pos);
	_length += strlen(chars);
	delete[] _data;
	_data = temp;
	return *this;
}

MyString & MyString::insert(int pos, MyString str) {
	this->insert(pos, str.chars());
	return *this;
}

MyString & MyString::insert(int pos, bool b) {
	this->insert(pos, (MyString)b);
	return *this;
}

MyString & MyString::insert(int pos, int num) {
	this->insert(pos, (MyString)num);
	return *this;
}

// �������� "count" �������� ������� � ������� "start"
MyString MyString::substr(int start, int count) const {
	MyString newStr;
	if ((start > _length) || (start + count > _length)) {
		return NULL;
	} else {
		newStr._length = count;
		newStr._data = new char[count + 1];
		strncpy(newStr._data, _data + start, count);
		newStr._data[count] = '\0';
	}
	return newStr;
}

// �������� ������� ������� "c"
int MyString::find(char c) const {
	char *p;
	p = strchr(_data, c);
	return p - _data;
}


/* destructor */
MyString::~MyString() {
	if (_data) delete[] _data;
}


/* operators */

// arithmetical
MyString MyString::operator + (const MyString & second) {
	MyString c = *this;
	c += second;
	return c;
}

MyString MyString::operator += (const MyString & second) {
	int len = _length + second._length;
	char * temp = new char[len + 1];
	for (int i = 0; i < len; i++) {
		temp[i] = '0';
	}
	temp[len] = '\0';
	strncpy(temp, _data, _length);
	strncpy(temp + _length, second._data, second._length);
	delete[] _data;
	_data = temp;
	_length = len;
	return *this;
}

// bool
MyString & MyString::operator = (const MyString & second) {
	if (second._length > 0) {
		_length = second._length;
		char *temp = new char[_length + 1];
		strcpy(temp, second._data);
		if (_data)
			delete[] _data;
		_data = temp;
	} else {
		_length = 0;
		if (_data)
			delete[] _data;
		_data = NULL;
	}
	return *this;
}

int MyString::operator == (const MyString & second) {
	return !strcmp(_data, second._data);
}

int MyString::operator != (const MyString & second) {
	return strcmp(_data, second._data);
}

// input & output
MyString MyString::readLine(std::istream & stream) {
	long int eoln;
	char inval[255];
	char *ptr;
	do {
		stream.getline(inval, 255);
		ptr = strchr(inval, '\0');
		eoln = ptr - inval;
	} while (!eoln && !stream.eof());

	if (_data) delete[] _data;
	_length = strlen(inval);
	_data = new char[_length + 1];
	strcpy(_data, inval);
	return *this;
}

std::istream & MyString::read(std::istream & stream) {
	char inval[255];
	inval[0] = '\0';
	char c;
	stream >> std::noskipws; //Do not skip whitespaces

	do {
		stream >> c;
		if (c != ' ' && c != '\n') {
			char temp[2];
			temp[0] = c;
			temp[1] = '\0';
			strcat(inval, temp);
		}
	} while (c != ' ' && c != '\n');

	_length = strlen(inval);
	_data = new char[_length + 1];
	strcpy(_data, inval);
	return stream;

}

std::ostream & MyString::write(std::ostream & stream) const {
	stream << _data;
	return stream;
}


std::ostream & operator << (std::ostream & stream, const MyString & el) {
	return el.write(stream);
}

std::istream & operator >> (std::istream & stream, MyString & el) {
	return el.read(stream);
}

