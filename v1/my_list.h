#include <iostream>
#ifndef __MY_LIST__
#define __MY_LIST__

template<typename list_type>
class my_list;

template<typename list_type>
class my_list_element {
	private:
	friend my_list<list_type>;
	list_type * _el;
	my_list_element * _next;

	public:
	/* init */
	my_list_element() : _el(NULL), _next(NULL) {};


	/* get & set */
	list_type * get_elem() {
		return _el;
	}

	void set_elem(list_type * el) {
		_el = el;
	}

	my_list_element<list_type> * get_next() {
		return _next;
	}

	void set_next(my_list_element<list_type> * next) {
		_next = next;
	}

	/* operators */
	friend std::ostream & operator << (std::ostream & stream, my_list_element<list_type> & el) {
		std::cout << *el.get_elem();
		return stream;
	}

	/* desctructor */
	~my_list_element<list_type>() {
		if (_el) delete _el;
		if (_next) delete _next;
	};
};


template<typename list_type>
class my_list {
	private:
	my_list_element<list_type> * _head;
	unsigned short int _sz;

	public:
	/* init */
	my_list() : _head(NULL), _sz(0) {};


	/* get & set */
	my_list_element<list_type> * front() {
		return _head;
	}

	int size() {
		return _sz;
	}

	bool empty() {
		return _head == NULL;
	}


	/* manipulate */
	void insert(list_type el) {
		my_list_element<list_type> * temp = new my_list_element<list_type>;
		temp->_el = new list_type(el);
		temp->_next = NULL;

		if (_head == NULL) _head = temp;
		else {
			my_list_element<list_type> * tempH = _head;
			for (int i = 0; i < _sz - 1; i++) tempH = tempH->_next;
			tempH->_next = temp;
		}
		_sz++;
	}

	void insert(list_type el, unsigned short int pos) {
		my_list_element<list_type> * temp = new my_list_element<list_type>;
		my_list_element<list_type> * tempH = _head;

		if (pos != 0) {
			for (int i = 0; i < _sz; i++) tempH = tempH->_next;
			temp->_el = new list_type(el);
			temp->_next = NULL;
			tempH->_next = temp;
		} else {
			temp->_next = _head;
			_head = temp;
		}
		sz++;
	}

	list_type del(int pos) {
		my_list_element<list_type> *temp = _head;
		list_type elem = NULL;
		if ((_head != NULL) && (pos <= _sz)) {
			for (int i = 0; i < pos - 1; i++) temp = temp->_next;
			elem = *temp->_next->get_elem();
			if (pos != _sz)
				temp->_next = temp->_next->_next;
			else
				temp->_next = NULL;
			_sz--;
			return elem;
		} else return NULL;
	};

	list_type * get_el(int pos) const {
		my_list_element<list_type> *temp = _head;

		if ((_head != NULL) && (pos <= _sz)) {
			for (int i = 0; i < pos; i++) temp = temp->_next;
			return temp->get_elem();
		};
	}

	void set_el(int pos, list_type el) const {
		set_el(pos, new list_type(el));
		return;
	}

	void set_el(int pos, list_type * el) const {
		my_list_element<list_type> *temp = _head;

		if ((_head != NULL) && (pos <= _sz)) {
			for (int i = 0; i < pos; i++) temp = temp->_next;
			temp->set_elem(el);
		};
		return;
	}

	void swap(int pos_1, int pos_2) {
		my_list_element<list_type> *temp, *pos_11, *pos_12;
		list_type * pos_3;

		if ((_head != NULL) && (pos_1 <= _sz) && (pos_2 <= _sz)) {
			temp = _head;
			for (int i = 0; i < pos_1; i++) temp = temp->_next;
			pos_11 = temp;

			temp = _head;
			for (int i = 0; i < pos_2; i++) temp = temp->_next;
			pos_12 = temp;

			pos_3 = pos_11->get_elem();
			pos_11->set_elem(pos_12->get_elem());
			pos_12->set_elem(pos_3);
		};
		return;
	}

	/* operators */
	friend std::ostream & operator << (std::ostream & stream, my_list<list_type> & el) {
		my_list_element<list_type> * temp = el.front();

		while (temp != NULL) {
			stream << *temp << endl;
			temp = temp->get_next();
		}

		stream << endl;
		return stream;
	}

	list_type operator[](int pos) const {
		return *get_el(pos);
	}


	/* desctructor */
	~my_list() {
		if (_head != NULL) delete _head;
	};





	/* SORT */
	void sort(int key, int type = 0) {
		// https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8
		// https://ru.wikibooks.org/wiki/%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0

		if (type == 0) sort_bubble(key);
		else if (type == 1) sort_quick(key, 0, _sz - 1);
		else if (type == 2) sort_insert(key);
		else if (type == 3) sort_merge(key, 0, _sz - 1);
	}
	void sort_bubble(int key) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%9F%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC

		for (int i = 0; i < _sz - 1; i++) {
			bool swapped = false;
			for (int j = 0; j < _sz - i - 1; j++) {
				if (*get_el(j) > *get_el(j + 1)) {
					swap(j, j + 1);
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}
	void sort_quick(int key, int first, int last) {
		// https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F

		int i = first, j = last;
		list_type x = *get_el((first + last) / 2);

		do {
			while (*get_el(i) < x) i++;
			while (*get_el(j) > x) j--;

			if (i <= j) {
				if (*get_el(i) > *get_el(j)) swap(i, j);
				i++;
				j--;
			}
		} while (i <= j);

		if (i < last)
			sort_quick(key, i, last);
		if (first < j)
			sort_quick(key, first, j);
	}

	void sort_insert(int key) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%B2%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%B2%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%92%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8

		list_type * x;
		int i = 0, j = 0;

		for (int i = 1; i < _sz; i++) {
			x = get_el(i);
			j = i;
			while (j > 0 && (*x < *get_el(j - 1))) {
				set_el(j, get_el(j - 1));
				j--;
			}
			set_el(j, x);
		}
	}

	void sort_merge(int key, int left, int right) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%A1%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC

		if (left + 1 >= right) return;
		int mid = (left + right) / 2;
		sort_merge(key, left, mid);
		sort_merge(key, mid, right);
		sort_merge_merge(left, mid, right);
	}
	void sort_merge_merge(int left, int mid, int right) {
		int it1 = 0, it2 = 0;
		my_list<list_type> result;
		for (int i = 0; i < right - left; i++) result.insert(0);

		while (left + it1 < mid && mid + it2 < right) {
			if (*get_el(left + it1) < *get_el(mid + it2)) {
				result.set_el(it1 + it2, *get_el(left + it1));
				it1 += 1;
			} else {
				result.set_el(it1 + it2, *get_el(mid + it2));
				it2++;

				while (*get_el(left + it1) < *get_el(mid)) {
					result.set_el(it1 + it2, *get_el(left + it1));
					it1++;

					while (*get_el(mid + it2) < *get_el(right)) {
						result.set_el(it1 + it2, *get_el(mid + it2));
						it2++;

						for (int i = 0; i < it1 + it2; i++)
							set_el(left + i, *result.get_el(i));
					}
				}
			}
		}
	}


	/* SEARCH */
	int search(int key, list_type value, int type = 0, char * filename = NULL) {
		if (type == 0) return search_linear(key, value);
		else if (type == 1) return search_dichotomy(key, value);
		else if (type == 2) return search_file(key, value, filename);
	}
	int search_linear(int key, list_type value) {
		for (int i = 0; i < _sz; i++) {
			if (*get_el(i) == value) return i;
		}
		return -1;
	}
	int search_dichotomy(int key, list_type value) {
		// https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A6%D0%B5%D0%BB%D0%BE%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA
		size_t left = 0, right = _sz - 1, mid = 0;

		sort(1, key);

		if (_sz == 0) return -1;							// ������ ����� = 0
		else if (*get_el(0) > value) return -1;				// �� ��� ����� �������
		else if (*get_el(_sz - 1) < value) return -1;		// �� ��� ����� �������

		while (left < right) {
			mid = left + (right - left) / 2;

			if (value <= *get_el(right)) right = mid;
			else left = mid + 1;
		}

		if (*get_el(right) == value) return right;
		else return -1;										// �� ������ ������
	}
	int search_file(int key, list_type value, char * filename) {
		return -1;
	}
};

#endif // __MY_LIST__
