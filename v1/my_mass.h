#include <iostream>
#ifndef __MY_MASS__
#define __MY_MASS__

template<typename mass_type>
class my_mass {
	private:
	mass_type ** _dt;
	int _sz;
	public:
	/* init */
	my_mass<mass_type>() : _dt(NULL), _sz(0) {};

	/* get & set */
	int size() const {
		return _sz;
	}

	bool empty() const {
		return _dt == NULL;
	}

	mass_type ** data() const {
		return _dt;
	}

	/* manipulate */

	void insert(mass_type el) {
		this->insert(_sz, el);
		return;
	}

	void insert(int pos, mass_type el) {
		int len = _sz + 1;
		mass_type ** temp = new mass_type*[len + 1];
		mass_type * newEl = new mass_type(el);
		for (int i = 0; i < pos; i++) {
			temp[i] = _dt[i];
		};
		temp[pos] = newEl;
		for (int i = pos + 1; i < _sz; i++) {
			temp[i] = _dt[i - 1];
		}
		delete[] _dt;
		_dt = temp;
		_sz = len;
		return;
	}

	mass_type del(int pos) {
		if ((_dt != NULL) && (pos <= _sz)) {
			mass_type elem = NULL;
			int len = _sz - 1;
			elem = *_dt[pos];
			mass_type ** temp = new mass_type*[len + 1];
			for (int i = 0; i < pos; i++) {
				temp[i] = _dt[i];
			};
			for (int i = pos + 1; i < _sz; i++) {
				temp[i - 1] = _dt[i];
			}
			delete[] _dt;
			_dt = temp;
			_sz = len;
			return elem;
		} else return NULL;
	};


	mass_type * get_el(int pos) const {
		if ((_dt != NULL) && (pos <= _sz)) {
			return _dt[pos];
		};
		return NULL;
	}

	void set_el(int pos, mass_type el) const {
		this->_dt[pos] = new mass_type(el);
		return;
	}

	void set_el(int pos, mass_type * el) const {
		this->_dt[pos] = el;
		return;
	}

	void swap(int pos_1, int pos_2) {
		mass_type * pos_3;
		pos_3 = this->_dt[pos_1];
		this->_dt[pos_1] = this->_dt[pos_2];
		this->_dt[pos_2] = pos_3;
		return;
	}

	/* operators */
	friend std::ostream & operator << (std::ostream & stream, const my_mass<mass_type> & el) {
		for (int i = 0; i < el.size(); i++) {
			std::cout << *el.data()[i] << endl;
		}

		std::cout << endl;
		return stream;
	};

	mass_type operator[](int pos) const {
		if (get_el(pos)) return *get_el(pos);
		else return NULL;
	}

	/* destructor */
	~my_mass<mass_type>() {
		delete[] _dt;
	};




	/* SORT */
	void sort(int key, int type = 0) {
		// https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B8
		// https://ru.wikibooks.org/wiki/%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F:%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0

		if (type == 0) sort_bubble(key);
		else if (type == 1) sort_quick(key, 0, _sz - 1);
		else if (type == 2) sort_insert(key);
	}
	void sort_bubble(int key) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%BF%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%9F%D1%83%D0%B7%D1%8B%D1%80%D1%8C%D0%BA%D0%BE%D0%BC

		for (int i = 0; i < _sz - 1; i++) {
			bool swapped = false;
			for (int j = 0; j < _sz - i - 1; j++) {
				if (*get_el(j) > *get_el(j + 1)) {
					swap(j, j + 1);
					swapped = true;
				}
			}
			if (!swapped) break;
		}
	}
	void sort_quick(int key, int first, int last) {
		// https://ru.wikipedia.org/wiki/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F_%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F

		int i = first, j = last;
		mass_type x = *get_el((first + last) / 2);

		do {
			while (*get_el(i) < x) i++;
			while (*get_el(j) > x) j--;

			if (i <= j) {
				if (*get_el(i) > *get_el(j)) swap(i, j);
				i++;
				j--;
			}
		} while (i <= j);

		if (i < last)
			sort_quick(key, i, last);
		if (first < j)
			sort_quick(key, first, j);
	}
	void sort_insert(int key) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%B2%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D0%B2%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%92%D1%81%D1%82%D0%B0%D0%B2%D0%BA%D0%B0%D0%BC%D0%B8

		mass_type * x;
		int i = 0, j = 0;

		for (int i = 1; i < _sz; i++) {
			x = get_el(i);
			j = i;
			while (j > 0 && (*x < *get_el(j - 1))) {
				set_el(j, get_el(j - 1));
				j--;
			}
			set_el(j, x);
		}
	}

	void sort_merge(int key, int left, int right) {
		// https://ru.wikipedia.org/wiki/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0_%D1%81%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%A1%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0/%D0%A1%D0%BB%D0%B8%D1%8F%D0%BD%D0%B8%D0%B5%D0%BC

		if (left + 1 >= right) return;
		int mid = (left + right) / 2;
		sort_merge(key, left, mid);
		sort_merge(key, mid, right);
		sort_merge_merge(left, mid, right);
	}

	void sort_merge_merge(int left, int mid, int right) {
		int it1 = 0, it2 = 0;
		my_mass<mass_type> result;
		for (int i = 0; i < right - left; i++) result.insert(0);

		while (left + it1 < mid && mid + it2 < right) {
			if (*get_el(left + it1) < *get_el(mid + it2)) {
				result.set_el(it1 + it2, *get_el(left + it1));
				it1 += 1;
			} else {
				result.set_el(it1 + it2, *get_el(mid + it2));
				it2++;

				while (*get_el(left + it1) < *get_el(mid)) {
					result.set_el(it1 + it2, *get_el(left + it1));
					it1++;

					while (*get_el(mid + it2) < *get_el(right)) {
						result.set_el(it1 + it2, *get_el(mid + it2));
						it2++;

						for (int i = 0; i < it1 + it2; i++)
							set_el(left + i, *result.get_el(i));
					}
				}
			}
		}
	}

	/* SEARCH */
	int search(int key, mass_type value, int type = 0, char * filename = NULL) {
		if (type == 0) return search_linear(key, value);
		else if (type == 1) return search_dichotomy(key, value);
		else if (type == 2) return search_file(key, value, filename);
	}
	int search_linear(int key, mass_type value) {
		for (int i = 0; i < _sz; i++) {
			if (*get_el(i) == value) return i;
		}
		return -1;
	}
	int search_dichotomy(int key, mass_type value) {
		// https://ru.wikipedia.org/wiki/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA
		// https://neerc.ifmo.ru/wiki/index.php?title=%D0%A6%D0%B5%D0%BB%D0%BE%D1%87%D0%B8%D1%81%D0%BB%D0%B5%D0%BD%D0%BD%D1%8B%D0%B9_%D0%B4%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA
		// https://ru.wikibooks.org/wiki/%D0%A0%D0%B5%D0%B0%D0%BB%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D0%B8_%D0%B0%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D0%BE%D0%B2/%D0%94%D0%B2%D0%BE%D0%B8%D1%87%D0%BD%D1%8B%D0%B9_%D0%BF%D0%BE%D0%B8%D1%81%D0%BA

		size_t left = 0, right = _sz - 1, mid = 0;

		sort(1, key);

		if (_sz == 0) return -1;							// ������ ����� = 0
		else if (*get_el(0) > value) return -1;				// �� ��� ����� �������
		else if (*get_el(_sz - 1) < value) return -1;		// �� ��� ����� �������

		while (left < right) {
			mid = left + (right - left) / 2;

			if (value <= *get_el(right)) right = mid;
			else left = mid + 1;
		}

		if (*get_el(right) == value) return right;
		else return -1;										// �� ������ ������
	}
	int search_file(int key, mass_type value, char * filename) {
		return -1;
	}
};

#endif // __MY_MASS__
