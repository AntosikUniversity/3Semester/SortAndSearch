#include <iostream>
#ifndef __DATA__
#define __DATA__

#ifndef __MY_STRING__
#include "MyString.h"
#endif

class student {
	private:
	MyString _first_name;
	MyString _last_name;
	unsigned short int _group_num;
	bool _gender;						// true - male, false - female
	unsigned short int _physics_score;
	unsigned short int _math_score;
	unsigned short int _russian_score;

	public:

	/* init */
	student(
		MyString fn = "",
		MyString ln = "",
		unsigned short int gn = 0,
		bool g = true,
		unsigned short int ps = 0,
		unsigned short int ms = 0,
		unsigned short int rs = 0
	) : _first_name(fn), _last_name(ln), _group_num(gn), _gender(g), _physics_score(ps), _math_score(ms), _russian_score(rs) {};

	/* get & set */
	MyString get_fn() const {
		return _first_name;
	};
	MyString get_ln() const {
		return _last_name;
	};
	unsigned short int get_gn() const {
		return _group_num;
	};
	bool get_g() const {
		return _gender;
	};
	unsigned short int get_ps() const {
		return _physics_score;
	};
	unsigned short int get_ms() const {
		return _math_score;
	};
	unsigned short int get_rs() const {
		return _russian_score;
	};


	/* operators */
	friend std::ostream & operator << (std::ostream & output, const student & el) {
		output
			<< " | " << el.get_ln() << ' ' << el.get_fn()
			<< " | " << el.get_gn()
			<< " | " << (MyString)el.get_g()
			<< " | " << el.get_rs()
			<< " | " << el.get_ms()
			<< " | " << el.get_ps()
			<< " | ";
		return output;
	};

	void write(std::ostream & output) {
		MyString res = generate_json();
		output.write(res.chars(), res.length());
		return;
	}

	friend std::istream & operator >> (std::istream & input, student & el) {
		char buf[256], fn[20], ln[20], g[10];
		unsigned short int gn, ps, ms, rs;

		input.getline(buf, 256);
		sscanf(buf, "{ %s , %s , %hu , %s , %hu , %hu , %hu }", fn, ln, &gn, g, &ps, &ms, &rs);

		el._first_name = fn;
		el._last_name = ln;
		el._group_num = gn;
		el._gender = (strcmp(g, "true") == 0 ? true : false);
		el._physics_score = ps;
		el._math_score = ms;
		el._russian_score = rs;

		return input;
	};

	MyString generate_json() const {
		MyString res;
		res
			.insert("{ ")
			.insert(_first_name)
			.insert(" , ")
			.insert(_last_name)
			.insert(" , ")
			.insert(_group_num)
			.insert(" , ")
			.insert(_gender)
			.insert(" , ")
			.insert(_physics_score)
			.insert(" , ")
			.insert(_math_score)
			.insert(" , ")
			.insert(_russian_score)
			.insert(" }")
			.insert('\n');

		return res;
	};


	/* desctructor */
	~student() {};
};

#endif // __DATA__
